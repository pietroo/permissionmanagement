package com.example.PermissionManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PermissionManagement {
	public static void main(String[] args) {
		SpringApplication.run(PermissionManagement.class, args);
	}
}



